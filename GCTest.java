import java.util.Arrays;
import java.util.ArrayList;

public class GCTest {
    public static enum StringCreationMethod {
        // Build the string inefficiently in a loop.
        SIMPLE,
        // Use a StringBuilder.
        BUILDER,
        // Use Java 11 repeat().
        REPEAT;
    }

    public static class StringCreator extends Thread {
        private final int stringLength;
        private final int threadTimeMs;
        private final StringCreationMethod method;

        public StringCreator(int stringLength, int threadTimeMs, StringCreationMethod method) {
            this.stringLength = stringLength;
            this.threadTimeMs = threadTimeMs;
            this.method = method;
        }

        public void run() {
            GCTest.loadString(stringLength, threadTimeMs, method);
        }
    }

    public static void main(String[] args) {
        System.out.println("Java program is running...");

        // Java 10 feature: var.
        var stringLength = 1000;
        var threadTimeMs = 5000;
        var method = StringCreationMethod.SIMPLE;

        var numThreads = 1000;
        var timeBetweenThreadsMs = 10;

        // Process arguments.
        for (var arg : args) {
            if (arg.startsWith("--string-length")) {
                stringLength = Integer.parseInt(arg.split("=")[1]);
            } else if (arg.startsWith("--thread-life")) {
                threadTimeMs = Integer.parseInt(arg.split("=")[1]);   
            } else if (arg.startsWith("--string-method")) {
                var inputMethod = arg.split("=")[1].toLowerCase();
                if (inputMethod.equals("simple")) {
                    method = StringCreationMethod.SIMPLE;
                } else if (inputMethod.equals("builder")) {
                    method = StringCreationMethod.BUILDER;
                } else if (inputMethod.equals("repeat")) {
                    method = StringCreationMethod.REPEAT;
                }
            } else if (arg.startsWith("--threads")) {
                numThreads = Integer.parseInt(arg.split("=")[1]);
            } else if (arg.startsWith("--time-between-threads")) {
                timeBetweenThreadsMs = Integer.parseInt(arg.split("=")[1]);
            }
        }

        // Print choices.
        System.out.println("Characters per String:");
        // Java 12 feature: String.indent() to prepend spaces.
        System.out.println(String.format("%d", stringLength).indent(4));
        System.out.println("Time each thread lives (ms):");
        System.out.println(String.format("%d", threadTimeMs).indent(4));
        System.out.println("Method for creating Strings:");
        System.out.println(String.format("%s", method).indent(4));
        System.out.println("Number of threads to create:");
        System.out.println(String.format("%d", numThreads).indent(4));
        System.out.println("Time to wait before starting the next thread (ms): ");
        System.out.println(String.format("%d", timeBetweenThreadsMs).indent(4));

        var threads = new ArrayList<Thread>();

        // Start up the threads.
        for (int i = 0; i < numThreads; i++) {
            var t = new StringCreator(stringLength, threadTimeMs, method);
            threads.add(t);
            t.start();

            try {
                Thread.sleep(timeBetweenThreadsMs);
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(1);
            }
        }

        // Join with all the threads to exit this main process when all the threads are finished.
        threads.forEach(t -> {
            try {
                t.join();
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(1);
            }
        });
       
        System.out.println("Java program ended.");
    }

    /**
     * Load a string into memory and hold onto it for a given time.
     *
     * @param size - The number of characters to load.
     * @param timeMs - The amount of time to sleep in milliseconds.
     * @param method - The {@link StringCreationMethod} to use.
     */
    public static void loadString(int size, int timeMs, StringCreationMethod method) {
        // The String to hold.
        var s = "";

        // Java 12 feature: improved switch.
        s = switch(method) {
            case SIMPLE -> {
                var tempString = "";
                // Add characters A - Z to the string until it reaches the specified size.
                for (var i = 0; i < size; i++) {
                    tempString += (char)('A' + (i % 26));
                }
                break tempString;
            }
            case BUILDER -> {
                var tempBuilder = new StringBuilder();
                for (var i = 0; i < size; i++) {
                    tempBuilder.append((char)('A' + (i % 26)));
                }
                break tempBuilder.toString();
            }
            case REPEAT -> {
                // Java 11 feature: String.repeat()
                break "A".repeat(size);
            }
            default -> {
                System.err.println("Invalid option: " + method);
                break "INVALID OPTION"; 
            }
        };

        try {
            Thread.sleep(timeMs);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    } 
}
