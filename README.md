# Garbage Collector Test

## About
Demo select Java 10-12 features by testing garbage collection.

## Setup
* Download/clone this repository.  
* Developed on CentOS Linux release 8.1.1911 (Core)  
* Download Java 12: `https://github.com/AdoptOpenJDK/openjdk12-binaries/releases/download/jdk-12.0.2%2B10/OpenJDK12U-jdk_x64_linux_hotspot_12.0.2_10.tar.gz`  

## Running
`./start.sh`  

## See all options
`./start.sh --help`  

## Details
This uses a shell script to prompt for inputs and start up a java process. The inputs are:  

* Garbage collector choice from: Epsilon, Serial, Parallel, Concurrent Mark & Sweep, ZGC, G1, and Shenandoah.  
* Number of characters in each String to generate (1 per thread).  
* Time for each thread to live.  
* Method to generate Strings: Simple loop, StringBuilder, or Java 11 repeat().  
* Number of threads to start.  
* Time between starting each thread.  

The Java process uses those configurations to load Strings into memory.  The script prints current memory usage while the process is running.  Then when it's finished, the script prints run time, max memory usage, and average memory usage.
