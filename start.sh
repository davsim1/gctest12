#!/bin/bash

check_interval_seconds=0.1
compile=false

string_length=1000
thread_time_ms=5000
method=simple

num_threads=1000
time_between_threads=10

declare -A gc_map
gc_map=(
    ["0) Epsilon - No garbage collection (Java 11 experimental)"]="-XX:+UnlockExperimentalVMOptions -XX:+UseEpsilonGC"
    ["1) Serial - Single core default in Java 8"]="-XX:+UseSerialGC"
    ["2) Parallel - Multi core default in Java 8"]="-XX:+UseParallelGC"
    ["3) Concurrent mark and sweep"]="-XX:+UseConcMarkSweepGC"
    ["4) ZGC - Low latency (Java 11 experimental)"]="-XX:+UnlockExperimentalVMOptions -XX:+UseZGC"
    ["5) G1 - Garbage first: modified mark and sweep (Java 9 - 12 default)"]="-XX:+UseG1GC"
    ["6) Shenandoah - Low pause time (Java 12 experimental)"]="-XX:+UnlockExperimentalVMOptions -XX:+UseShenandoahGC"
)

IFS=$'\n' sorted_gc_keys=($(sort <<<"${!gc_map[*]}")); unset IFS

for arg in $@; do
    case $arg in
        *help)
            man ./manpage/GCTest
            exit 0
            ;;
        *compile)
            compile=true
        ;;
    esac
done

# Compile the class if necessary.
if [[ $compile = true ]]; then
    echo "Compiling..."
    rm -f ./*.class
    javac --enable-preview -source 12 GCTest.java
    if [[ $? -ne 0  ]]; then
        echo "Failed to compile."
        exit 1
    fi
fi

if [[ ! -f GCTest.class ]]; then
    echo "Could not find compiled class.  Run again with --compile."
    exit 1
fi

# Print available garbage collectors.
for gc_option in "${sorted_gc_keys[@]}"; do
    echo $gc_option
done

echo ""
read -p "Garbage collector to use? [0 - $((${#sorted_gc_keys[@]} - 1))]: " gc_input_key

gc_choice="${gc_map[${sorted_gc_keys[$gc_input_key]}]}"

# Take all settings if necessary.
echo ""
read -p "Want to tune all settings? ('n' to use defaults) [y/n]: " tune_settings

if [[ $tune_settings = "y" ]]; then
    echo "This generates 1 String per thread."
    read -p "Number of characters in each String to generate? (default 1000): " string_length
    read -p "Time for each thread to live in ms? (default 5000): " thread_time_ms
    read -p "Method to generate Strings? 'simple' to use a loop, 'builder' to use StringBuilder, 'repeat' to use Java 11 String.repeat(): " method
    read -p "Total number of threads to start? (default 1000): " num_threads
    read -p "Time between starting each thread in ms? (default 10): " time_between_threads
fi

# Run the Java program in the background and print runtime when it's done.
time java $gc_choice \
    --enable-preview GCTest \
    --string-length=$string_length \
    --thread-life=$thread_time_ms \
    --string-method=$method \
    --threads=$num_threads \
    --time-between-threads=$time_between_threads \
    &

sleep 0.1

java_pid=$(pgrep java)

if [[ $? -ne 0 ]]; then
    echo "Failed to start program."
    exit 1
fi

echo "Started process with pid $java_pid."

max_mem=0
count=0
sum=0

# Estimate run time, multiply by 2 for saftey then divide by 100
# because the following loop checks every 100ms.
iterations=$((((num_threads * time_between_threads + thread_time_ms) * 2) / 100))

for i in $(seq 0 $iterations); do
    rss_output=$(ps -o rss= $java_pid)

    if [[ $? -eq 0 ]]; then
        echo "Memory = $rss_output KB"
        if [[ $rss_output -gt $max_mem ]]; then
            max_mem=$rss_output
        fi
        ((count++))
        ((sum+=rss_output))
    else
        echo "Detected that the process ended."
        break
    fi

    sleep $check_interval_seconds
done
echo ""
echo "For garbage collector '${sorted_gc_keys[$gc_input_key]}':"
echo ""
echo "Maximum memory used: $max_mem KB."
echo "Average memory used: $(($sum / $count)) KB."
